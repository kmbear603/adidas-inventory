"use strict"
const express = require('express');
const fs = require("fs");
const os = require("os");
const adidas = require("./server/adidas.js");
const currency = require("./server/currency.js");

const PORT = process.env.PORT || 8080;

const app = express();

function readTextFile(path){
	return new Promise((resolve, reject)=>{
		fs.readFile(path, "utf8", (err, html)=>{
			if (err)
				return reject(err);
			resolve(html);
		});
	});
}

app.get("/*.js", (req, res)=>{
	readTextFile("./build" + req.path).then(txt=>{
		res.status(200).type("javascript").send(txt);
	}).catch(err=>{
		res.status(500).send("error");
	});
});

app.get("/*.css", (req, res)=>{
	readTextFile("./build" + req.path).then(txt=>{
		res.status(200).type("css").send(txt);
	}).catch(err=>{
		res.status(500).send("error");
	});
});

app.get("/*.ico", (req, res)=>{
	res.status(404).send("Not found");
});

app.get("/:productIds?", (req, res)=>{
	readTextFile("./build/index.html").then(html=>{
		res.status(200).type("html").send(html);
	}).catch(err=>{
		res.status(500).send("error");
	});
});

app.get("/api/store", (req, res)=>{
	adidas.getSupportedStore().then(obj=>{
		res.status(200).type("json").send(obj);
	}).catch(err=>{
		res.status(500).send("error");
	});
});

app.get("/api/product/:series", (req, res)=>{
	adidas.fetchAllProductId(req.params.series).then(obj=>{
		res.status(200).type("json").send(obj);
	}).catch(err=>{
		res.status(500).send("error");
	});
});


app.get('/api/currency/:base', (req, res)=>{
	currency.getRate(req.params.base).then(obj=>{
		res.status(200).type("json").send(obj);
	}).catch(err=>{
		res.status(500).type("json").send(err);
	});
});

app.get('/api/all/:productId', (req, res)=>{
	adidas.fetchAllInventory(req.params.productId).then(obj=>{
		res.status(200).type("json").send(obj);		
	}).catch(err=>{
		res.status(500).type("json").send(err);
	});
});

app.get('/api/:storeId/:productId', (req, res)=>{
	adidas.fetchInventory(req.params.storeId, req.params.productId).then(obj=>{
		res.status(200).type("json").send(obj);		
	}).catch(err=>{
		res.status(500).type("json").send(err);
	});
});

app.get("*", (req, res)=>{
	res.status(404).send("What are you looking for?");		
});

//app.use("/", express.static("build"));

app.listen(PORT, ()=>{
	  console.log("listening on port " + PORT + "!")
});
