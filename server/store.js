module.exports = [
    {
        id: "us",
        name: "United States",
        pages: {
            ultraboost: "http://www.adidas.com/us/ultraboost"
        },
        url: "http://www.adidas.com/us",
        variantUrl: "http://www.adidas.com/on/demandware.store/Sites-adidas-US-Site/en_US/Product-GetVariants",
		addCartUrl: "http://www.adidas.com/on/demandware.store/Sites-adidas-US-Site/en_US/Cart-MiniAddProduct",
        mediaDomain: "http://demandware.edgesuite.net/sits_pod20-adidas/dw/image/v2/aaqx_prd/on/demandware.static/-/Sites-adidas-products/en_US/dwbe678954/zoom",
        currency: "USD",
        dollar: "$",
        sizeUnit: "US"
    }, {
        id: "ca",
        name: "Canada",
        pages: {
            ultraboost: "http://www.adidas.ca/en/ultra_boost"
        },
        url: "http://www.adidas.ca/en",
        variantUrl: "http://www.adidas.ca/on/demandware.store/Sites-adidas-CA-Site/en_CA/Product-GetVariants",
		addCartUrl: "http://www.adidas.ca/on/demandware.store/Sites-adidas-CA-Site/en_CA/Cart-MiniAddProduct",
        mediaDomain: "http://demandware.edgesuite.net/sits_pod20-adidas/dw/image/v2/aaqx_prd/on/demandware.static/-/Sites-adidas-products/default/dwd2af1885/zoom",
        currency: "CAD",
        dollar: "C$",
        sizeUnit: "US",
        //hidden: true
    }, {
        id: "uk",
        name: "United Kingdom",
        pages: {
            ultraboost: "http://www.adidas.co.uk/ultra_boost"
        },
        url: "http://www.adidas.co.uk",
        variantUrl: "http://www.adidas.co.uk/on/demandware.store/Sites-adidas-GB-Site/en_GB/Product-GetVariants",
		addCartUrl: "http://www.adidas.co.uk/on/demandware.store/Sites-adidas-GB-Site/en_GB/Cart-MiniAddProduct",
        mediaDomain: "http://demandware.edgesuite.net/sits_pod14-adidas/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dw61d1f696/zoom",
        currency: "GBP",
        dollar: "£",
        sizeUnit: "UK"
    }, {
        id: "eu",
        name: "Europe",
        pages: {
            ultraboost: "http://www.adidas.de/ultra_boost"
        },
        url: "http://www.adidas.de",
        variantUrl: "http://www.adidas.nl/on/demandware.store/Sites-adidas-DE-Site/de_DE/Product-GetVariants",
        addCartUrl: "http://www.adidas.nl/on/demandware.store/Sites-adidas-DE-Site/de_DE/Cart-MiniAddProduct",
        mediaDomain: "http://demandware.edgesuite.net/sits_pod14-adidas/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dw61d1f696/zoom",
        currency: "EUR",
        dollar: "€",
        sizeUnit: "EU",
        hidden: true
    },
    {
        id: "de",
        name: "Germany",
        pages: {
            ultraboost: "http://www.adidas.de/ultra_boost"
        },
        url: "http://www.adidas.de",
        variantUrl: "http://www.adidas.nl/on/demandware.store/Sites-adidas-DE-Site/de_DE/Product-GetVariants",
        addCartUrl: "http://www.adidas.nl/on/demandware.store/Sites-adidas-DE-Site/de_DE/Cart-MiniAddProduct",
        mediaDomain: "http://demandware.edgesuite.net/sits_pod14-adidas/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dw61d1f696/zoom",
        currency: "EUR",
        dollar: "€",
        sizeUnit: "EU",
        hidden: true
    },
    {
        id: "fr",
        name: "France",
        pages: {
            ultraboost: "http://www.adidas.fr/ultra_boost"
        },
        url: "http://www.adidas.fr",
        variantUrl: "http://www.adidas.nl/on/demandware.store/Sites-adidas-FR-Site/fr_FR/Product-GetVariants",
        addCartUrl: "http://www.adidas.nl/on/demandware.store/Sites-adidas-FR-Site/fr_FR/Cart-MiniAddProduct",
        mediaDomain: "http://demandware.edgesuite.net/sits_pod14-adidas/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dw73c32885/zoom",
        currency: "EUR",
        dollar: "€",
        sizeUnit: "EU",
        hidden: true
    },
    {
        id: "au",
        name: "Australia",
        pages: {
            ultraboost: "http://www.adidas.com.au/ultra_boost"
        },
        url: "http://www.adidas.com.au",
        variantUrl: "http://www.adidas.com.au/on/demandware.store/Sites-adidas-AU-Site/en_AU/Product-GetVariants",
        addCartUrl: "http://www.adidas.com.au/on/demandware.store/Sites-adidas-AU-Site/en_AU/Cart-MiniAddProduct",
        mediaDomain: "http://demandware.edgesuite.net/sits_pod14-adidas/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dw9c33f40c/zoom",
        currency: "AUD",
        dollar: "A$",
        sizeUnit: "US"
    },
    {
        id: "nz",
        name: "New Zealand",
        pages: {
            ultraboost: "http://www.adidas.co.nz/ultra_boost"
        },
        url: "http://www.adidas.co.nz",
        variantUrl: "http://www.adidas.co.nz/on/demandware.store/Sites-adidas-NZ-Site/en_NZ/Product-GetVariants",
        addCartUrl: "http://www.adidas.co.nz/on/demandware.store/Sites-adidas-NZ-Site/en_NZ/Cart-MiniAddProduct",
        mediaDomain: "http://demandware.edgesuite.net/sits_pod14-adidas/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dw242377b7/zoom",
        currency: "NZD",
        dollar: "$",
        sizeUnit: "US",
        hidden: true
    },
    {
        id: "jp",
        name: "Japan",
        pages: {
        },
        url: "http://www.adidas.jp",
        productUrl: "http://shop.adidas.jp/products/",
        mediaDomain: "http://demandware.edgesuite.net/sits_pod20-adidas/dw/image/v2/aaqx_prd/on/demandware.static/-/Sites-adidas-products/en_US/dwbe678954/zoom",   // use US
        currency: "JPY",
        dollar: "¥",
        sizeUnit: "JP",
        //hidden: true
    },
    {
        id: "kr",
        name: "Korean",
        pages: {
        },
        url: "http://www.adidas.co.kr",
        sizingUrl: "http://shop.adidas.co.kr/PF020401.action?command=LIST_6&SITE_CD=1&PROD_CD=",
        infoUrl: "http://shop.adidas.co.kr/PF021003.action?command=LIST&todayProd=",
        productUrl: "http://shop.adidas.co.kr/PF020401.action?PROD_CD=",
        mediaDomain: "http://demandware.edgesuite.net/sits_pod20-adidas/dw/image/v2/aaqx_prd/on/demandware.static/-/Sites-adidas-products/en_US/dwbe678954/zoom",   // use US
        currency: "KRW",
        dollar: "원",
        sizeUnit: "KR",
        //hidden: true
    }
]
