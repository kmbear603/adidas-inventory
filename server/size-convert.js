"use strict"

// from http://www.adidas.com/us/help-section-size_charts.html
const ADULT_CONFIG = {
    adidas: {
        name: "Adidas",
        min: 530,
        max: 740,
        step: 10
    },
    us_m: {
        name: "US_M",
        min: 4,
        max: 14.5,
        step: 0.5
    },
    us_w: {
        name: "US_W",
        min: 5,
        max: 13,
        step: 0.5
    },
    uk: {
        name: "UK",
        min: 3.5,
        max: 14,
        step: 0.5
    },
    eu: {
        name: "EU",
        min: 36,
        max: 50,
        step: 2/3
    },
    jp: {
        name: "JP",
        min: 22,
        max: 32.5,
        step: 0.5
    },
    kr: {
        name: "KR",
        min: 220,
        max: 325,
        step: 5
    }
};


function populate(config){
    const ret = [];
    
    const calc = (obj, i)=>{
        const v = obj.min + (i * obj.step);
        if (v > obj.max)
            return null;

        if (obj.name === "EU" && v - Math.floor(v) > 0){
            const r = v - Math.floor(v);
            if (r < 0.5)    // 1/3
                return Math.floor(v) + " 1/3";
            else    // 2/3
                return Math.floor(v) + " 2/3";
        }
        return v + "";
    };
    
    for (var i = 0; i < 500; i++){
        const code = calc(config.adidas, i);
        if (!code)
            break;

        const obj = { code: code };
        for (var country in config){
            if (country == "adidas")
                continue;
            const v = calc(config[country], i);
            if (v)
                obj[country] = v;
        }

        ret.push(obj);
    }

    return ret;
}

const ADULT_ARRAY = populate(ADULT_CONFIG);

module.exports = {
    lookupAdidasCode: function(code){
        code = code + "";
        const obj = ADULT_ARRAY.find(o=>{
            return o.code == code
        });
        return obj;
    },
    
    lookupJPSize: function(jp_size){
        if (typeof jp_size == "string")
            jp_size = parseFloat(jp_size);
            
        const obj = ADULT_ARRAY.find(o=>{
            return o.jp && parseFloat(o.jp) == jp_size;
        });
        
        return obj;
    },
    
    lookupKRSize: function(kr_size){
        if (typeof kr_size == "string")
            kr_size = parseInt(kr_size);
            
        const obj = ADULT_ARRAY.find(o=>{
            return o.kr && parseFloat(o.kr) == kr_size;
        });
        
        return obj;
    },
};
