"use strict"

// api.fixer.io wrapper

var request = require("superagent")

module.exports = {
    getRate: function(base_currency, target_currency){
        base_currency = base_currency.toUpperCase();
        if (target_currency)
            target_currency = target_currency.toUpperCase();
        
        return new Promise((resolve, reject)=>{
            if (base_currency == target_currency){
                const ret = {
                    base: base_currency,
                    time: new Date().getTime()
                };
                ret[target_currency] = 1;
                return resolve(ret);
            }

            var req = request
                    .get("http://api.fixer.io/latest")
                    .query({ base: base_currency });

            if (target_currency)
                req = req.query({ symbols: target_currency });

            req.then(res=>{
                const obj = JSON.parse(res.text);
                var ret = {
                    base: base_currency,
                    time: new Date().getTime()
                };
                if (target_currency)
                    ret[target_currency] = obj.rates[target_currency];
                else {
                    for (var c in obj.rates)
                        ret[c] = obj.rates[c];
                }
                resolve(ret);
            })
            .catch(err=>{
                reject(err);
            });
        });
    }
};