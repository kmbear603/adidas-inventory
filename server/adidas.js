"use strict"
const request = require("superagent");
const cheerio = require("cheerio");
const allStores = require("./store.js");
const sizeConversion = require("./size-convert.js");

const stores = allStores.filter(s=>{
    return !s.hidden;
});

// ugly code to avoid memory leak
var GC_TIMER = null;
var warnedNoGC = false;
function forceGCAfterUsingCheerio(){
    if (global.gc && typeof global.gc === "function"){
        if (GC_TIMER)
            clearTimeout(GC_TIMER);

        GC_TIMER = setTimeout(()=>{
            global.gc();
            console.log("forced GC");
        }, 3000);
    }
    else if (!warnedNoGC){
        warnedNoGC = true;
        console.log("GC is not enabled, you may see memory leak");
	    console.log("Tips: run this server again with --expose-gc option");
    }
}

function lookupStore(id){
    return stores.find(c=>{ return c.id == id; });
}

function getSupportedStore(){
    return new Promise((resolve, reject)=>{
        resolve(stores.map(c=>{
            return {
                id: c.id,
                name: c.name
            };
        }));
    });
}

function fetchProductId(store_id, series){
    return new Promise((resolve, reject)=>{
        const return_obj = products=>{
            return resolve({
                store: store_id,
                products: products
            });
        };
        
        const return_error = err_msg=>{
            return resolve({
                store: store_id,
                products: [],
                error: err_msg
            });
        };
        
        const store = lookupStore(store_id);
        if (!store)
            return return_error("unknown store " + store_id);
        
        series = series.toLowerCase();
        const page_url = store.pages[series];
        if (!page_url)
            return return_error("unsupported series " + series + " for store " + store_id);
        
        const all = {};
        
        const work = (cursor, trial)=>{
            const this_page_url = page_url + (cursor > 0 ? "?start=" + cursor : "");
            
            request.get(this_page_url).timeout(3 * 1000).then(res=>{
                var count = 0;
                const $ = cheerio.load(res.text);
                $(".image.plp-image-bg a").each((i, a)=>{
                    const pid = $(a).attr("data-track");
                    if (pid && !all.hasOwnProperty(pid)){
                        all[pid] = true;
                        count++;
                    }
                });
                
                forceGCAfterUsingCheerio();
                
                if (count > 0)
                    work(cursor + count, 0);
                else {
                    const ret = [];
                    for (var pid in all)
                        ret.push(pid);
                    return_obj(ret);
                }
            })
            .catch(err=>{
                if (trial == 3)
                    return return_error(err);
                work(cursor, trial + 1);
            });
        }
        
        work(0, 0);
    });
}

function fetchAllProductId(series){
    return new Promise((resolve, reject)=>{
        Promise.all(stores.map(s=>{ return fetchProductId(s.id, series); })).then(objs=>{
            const combined = {};
            
            objs.forEach(obj=>{
                obj.products.forEach(pid=>{
                    if (!combined.hasOwnProperty(pid)){
                        combined[pid] = true;
                    }
                });
            });

            const ret = [];
            for (var pid in combined)
                ret.push(pid);
 
            resolve(ret);
        }).catch(err=>{
            reject(err);
        });
    });
}

function getImageUrl(store_id, product_id){
    const store = lookupStore(store_id) || stores[0];
    return store.mediaDomain + "/" + product_id + "_01_standard.jpg?sw=300&sfrm=jpg";
}

function randomIP(){
    return Math.floor(Math.random() * 256) + "." + Math.floor(Math.random() * 256) + "." + Math.floor(Math.random() * 256) + "." + Math.floor(Math.random() * 256);
}

function fetchInventoryKR(product_id){
    const store_id = "kr";
    
    return new Promise((resolve, reject)=>{
        const store = lookupStore(store_id);
        if (!store)
            return reject("unknown store " + store_id);

        const generate_return_object = (inventories, product_url, name, gender, colorways)=>{
            const obj = {
                product: product_id,
                store: store_id,
                inventories: inventories
            };
            if (product_url)
                obj.productUrl = product_url;
            if (name)
                obj.name = name;
            if (gender)
                obj.gender = gender;
            if (colorways)
                obj.colorways = colorways;
            obj.image = getImageUrl(store_id, product_id);
            return obj;
        };
        
        const generate_error_object = err_msg=>{
            const ret = generate_return_object([]);
            ret.error = err_msg;
            return ret;
        };

        const product_url = store.productUrl + product_id;
        
        Promise.all([
            request.get(store.infoUrl + "%27" + product_id + "%27"),
            request.get(store.sizingUrl + product_id)
        ]).then(ress=>{
            // sizing
            const invs = [];
            {
                const res = ress[1];
                
                const json = JSON.parse(res.text);
                
                if (json && json.list && json.list.list){
                    json.list.list.forEach(itm=>{
                        const size = itm.OPTION_NM;
                        const size_obj = sizeConversion.lookupKRSize(size);
                        
                        const inv = { id: product_id + "_" + size_obj.code };
                        inv.size = itm.OPTION_NM;
                        inv.quantity = parseInt(itm.REAL_INVNTRY_QTY);
                        inv.dollar = store.dollar;
                        inv.sizeUnit = store.sizeUnit;
                        inv.currency = store.currency;
                        invs.push(inv);
                    });
                }
            }
            
            // info
            var name = null;
            var price = null, price_sale = null;
            {
                const res = ress[0];
                const json = JSON.parse(res.text);

                if (json && json.todayList){
                    json.todayList.forEach(e=>{
                        if (e.PROD_CD.toLowerCase() == product_id.toLowerCase()){
                            name = e.PROD_NM_KOR;
                            price = parseFloat(e.NORML_PRICE.replace(/[^0-9.]/g, ""));
                            price_sale = parseFloat(e.SALES_PRICE.replace(/[^0-9.]/g, ""));
                        }
                    });
                }
            }
            
            if (price || price_sale){
                invs.forEach(inv=>{
                    if (price)
                        inv.price = price;
                    if (price_sale && price_sale != price)
                        inv.priceSale = price_sale;
                });
            }
            
            resolve(generate_return_object(invs, product_url, name, null, null));
        })
        .catch(err=>{
            resolve(generate_error_object("exception"));
        });
    });
}

function fetchInventoryJP(product_id){
    const store_id = "jp";
    
    return new Promise((resolve, reject)=>{
        const store = lookupStore(store_id);
        if (!store)
            return reject("unknown store " + store_id);

        const generate_return_object = (inventories, product_url, name, gender, colorways)=>{
            const obj = {
                product: product_id,
                store: store_id,
                inventories: inventories
            };
            if (product_url)
                obj.productUrl = product_url;
            if (name)
                obj.name = name;
            if (gender)
                obj.gender = gender;
            if (colorways)
                obj.colorways = colorways;
            obj.image = getImageUrl(store_id, product_id);
            return obj;
        };
        
        const generate_error_object = err_msg=>{
            const ret = generate_return_object([]);
            ret.error = err_msg;
            return ret;
        };

        const product_url = store.productUrl + product_id;
        request.get(product_url).then(res=>{
            const html = res.text;
            
            // get prices
            var curr_price = null, ori_price = null;
            {
                const $ = cheerio.load(html);
                curr_price = parseFloat($("span.txt_tax").text().replace(/[^0-9.]/g, ""));
                const ori = $("p.proper");
                ori_price = ori.length > 0 ? parseFloat(ori.text().replace(/[^0-9.]/g, "")) : curr_price;
                forceGCAfterUsingCheerio();
            }

            // get quantities and item name
            const lines = html.split('\n');
            
            // item name is stored in the line
            // _item_name['BY2550'] = "UltraBOOST Uncaged";
            
            // all info are stored in javascript array _kcod_zaik
            //_kcod_zaik['BY2550'] = new Array();
            //_kcod_zaik_sort['BY2550'] = new Array();
            //_kcod_zaik['BY2550']['27'] = new Array();
            //_kcod_zaik['BY2550']['27']['size_name'] = '22.0cm';
            //_kcod_zaik['BY2550']['27']['size_name2'] = '22.0cm';
            //_kcod_zaik['BY2550']['27']['stock_label'] = '×';
            //_kcod_zaik['BY2550']['27']['cc_stock'] = '0';
            //_kcod_zaik['BY2550']['27']['stock_status'] = 'sold';
            //_kcod_zaik['BY2550']['27']['ec_shop_sold_state'] = 'sold_all';
            //_kcod_zaik_sort['BY2550'][0] = '27';
            //_kcod_zaik['BY2550']['28'] = new Array();
            //_kcod_zaik['BY2550']['28']['size_name'] = '22.5cm';
            //_kcod_zaik['BY2550']['28']['size_name2'] = '22.5cm';
            //_kcod_zaik['BY2550']['28']['stock_label'] = '×';
            //_kcod_zaik['BY2550']['28']['cc_stock'] = '0';
            //_kcod_zaik['BY2550']['28']['stock_status'] = 'sold';
            //_kcod_zaik['BY2550']['28']['ec_shop_sold_state'] = 'sold_all';
            //_kcod_zaik_sort['BY2550'][1] = '28';
            
            const extract_content = line=>{
                const ret = []; // first few elements are value in brackets, last element is value after equal sign
                
                const equal = line.indexOf('=');
                if (equal < 0)
                    return null;

                var cursor = 0;
                while (true){
                    const i = line.indexOf('[', cursor);
                    if (i < 0 || i > equal)
                        break;

                    const j = line.indexOf(']', i + 1);
                    if (j < 0 || j > equal)
                        break;

                    ret.push(line.substr(i + 1, j - i - 1).trim().replace(/['"]/g, ""));
                    cursor = j + 1;
                }
                
                ret.push(line.substr(equal + 1).trim().replace(/['";]/g, ""));
                
                return ret;
            };
            
            const invs = [];
            var item_name = null;

            lines.forEach(line=>{
                if (line.indexOf("_item_name[") == 0){
                    const tokens = extract_content(line);
                    if (tokens && tokens.length == 2){
                        const this_product_id = tokens[0];
                        if (this_product_id.toLowerCase() == product_id.toLowerCase())
                            item_name = tokens[1];
                    }
                }
                else if (line.indexOf("_kcod_zaik[") == 0){
                    const tokens = extract_content(line);
                    if (tokens && tokens.length === 4){
                        const this_product_id = tokens[0];
                        if (this_product_id.toLowerCase() == product_id.toLowerCase()){
                            const index = tokens[1];
                            const key = tokens[2];
                            const val = tokens[3];

                            var inv = invs.find(i=>{ return i.index == index; });
                            if (!inv){
                                inv = { index: index };
                                invs.push(inv);
                            }

                            if (key == "size_name"){
                                inv.size = val;
                                const jp_size = parseFloat(val.replace(/[^0-9.]/g, ""));
                                const size_obj = sizeConversion.lookupJPSize(jp_size);
                                inv.id = this_product_id + "_" + size_obj.code;
                            }
                            else if (key == "stock_label"){
                                if (val == "×")
                                    inv.quantity = 0;
                                else {
                                    const q = parseInt(val);
                                    if (!isNaN(q))
                                        inv.quantity = q;
                                    else
                                        inv.inStock = true;
                                }
                            }
                        }
                    }
                }
            });
            
            invs.forEach(inv=>{
                delete inv.index;
                
                inv.price = ori_price;
                if (curr_price != ori_price)
                    inv.priceSale = curr_price;
                    
                inv.sizeUnit = store.sizeUnit;
                inv.currency = store.currency;
                inv.dollar = store.dollar;
            });

            resolve(generate_return_object(invs, product_url, item_name, null, null));
        })
        .catch(err=>{
            resolve(generate_error_object("exception"));
        });
    });
}

function fetchInventory(store_id, product_id){
    if (store_id === "jp")
        return fetchInventoryJP(product_id);
    else if (store_id === "kr")
        return fetchInventoryKR(product_id);
    
    return new Promise((resolve, reject)=>{
        const generate_return_object = (inventories, product_url, name, gender, colorways)=>{
            const obj = {
                product: product_id,
                store: store_id,
                inventories: inventories
            };
            if (product_url)
                obj.productUrl = product_url;
            if (name)
                obj.name = name;
            if (gender)
                obj.gender = gender;
            if (colorways)
                obj.colorways = colorways;
            obj.image = getImageUrl(store_id, product_id);
            return obj;
        };
        
        const generate_error_object = err_msg=>{
            const ret = generate_return_object([]);
            ret.error = err_msg;
            return ret;
        };

        const store = lookupStore(store_id);
        if (!store)
            return reject(generate_error_object("not found"));
            
        const url = store.variantUrl + "?pid=" + product_id;
        
        //const ip = randomIP();
        const do_get = (get_count)=>{   // need retry because sometimes adidas server returns html instead of json
            if (get_count === 3)
                return resolve(generate_error_object("http failure"));
                
            request.get(url)
                .redirects(0)
                /*.set("X-Forwarded-For", ip)
                .set("Proxy-Client-IP", ip)
                .set("WL-Proxy-Client-IP", ip)
                .set("HTTP_X_FORWARDED_FOR", ip)
                .set("HTTP_X_FORWARDED", ip)
                .set("HTTP_X_CLUSTER_CLIENT_IP", ip)
                .set("HTTP_CLIENT_IP", ip)
                .set("HTTP_FORWARDED_FOR", ip)
                .set("HTTP_FORWARDED", ip)
                .set("HTTP_VIA", ip)
                .set("REMOTE_ADDR", ip)*/
                .timeout(3000)
                //.set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36")
                .set("Accept", "*/*")
                .set("Accept-Encoding", "gzip, deflat")
                .set("Connection", "keep-alive")
                .then(res=>
            {
                if (res.status != 200)
                    return resolve(generate_error_object("http status=" + res.status));
    
                var json;
                try {
                    json = JSON.parse(res.text);
                }
                catch (e){
//console.log(store_id, product_id, res.status);
                    return do_get(get_count + 1);
                }
                
                if (!json)
                    return do_get(get_count + 1);
                else if (!json.variations || !json.variations.variants)
                    return resolve(generate_error_object("wrong format"));
                
                const in_stock_variants = [];
                var inventories = json.variations.variants.map(v=>{
                    const obj = {
                        id: v.id,
                        size: v.attributes.size,
                        sizeUnit: store.sizeUnit,
                        quantity: v.ATS,
                        currency: store.currency,
                        dollar: store.dollar,
                        //image: getImageUrl(store.id, product_id)
                    };
                    
                    const sale = parseFloat(v.pricing.sale);
                    const price = parseFloat(v.pricing.standard);
                    obj.price = price;
                    if (sale != price)
                        obj.priceSale = sale;
                    
                    if (obj.quantity > 0)
                        in_stock_variants.push(v.id);
                    
                    return obj;
                });
                
                if (in_stock_variants.length === 0)
                    return resolve(generate_return_object(inventories));
    
                const change_quantity = (vid, q)=>{
                    const inv = inventories.find(i=>{ return i.id === vid; })
                    if (inv)
                        inv.quantity = q;
                };
    
                const set_add_to_cart_url = (vid)=>{
                    const inv = inventories.find(i=>{ return i.id === vid; });
                    if (inv)
                        inv.addToCartUrl = store.addCartUrl + "?pid=" + vid + "&Quantity=1&ajax=true&layer=Add%20To%20Bag%20overlay";
                };
    
                // make sure it still orderable by adding to cart
                const add_cart = (cursor)=>{
                    if (cursor >= in_stock_variants.length)
                        return resolve(generate_return_object(inventories));
    
                    const vid = in_stock_variants[cursor];
    
                    request.get(store.addCartUrl + "?pid=" + vid + "&Quantity=1&ajax=true").then(res=>{
                        // when failure, it is a json, otherwise it is a html
                        try {
                            const err_json = JSON.parse(res.text);
                            
                            // no exception, it is failure
                            if (err_json.error == "INVALID_CAPTCHA"){
                                // this product requires captcha, cant do anything here, just return
                                return resolve(generate_return_object(inventories));
                            }
                            else if (err_json.error == "OUT-OF-STOCK"){
                                change_quantity(vid, 0);
                                return add_cart(cursor + 1);
                            }
                            
                            // just return for all other errors
                            return resolve(generate_return_object(inventories));
                        }
                        catch (e){ }
                        
                        const $ = cheerio.load("<div>" + res.text + "</div>");
                        const encoded = $("div[data-component='pagecontext/Context']").text();
                        const str = decodeURIComponent(encoded);
                        const json = JSON.parse(str);
    
                        if (!json["product_status**"] || json["product_status**"] != "IN STOCK"){
                            change_quantity(vid, 0);
                            return add_cart(cursor + 1);
                        }
                        
                        const product_url = json["product_url**"];
                        const product_name = json["product_name**"];
                        const product_gender = json["product_gender**"];
                        const product_colorways = json["product_colorways**"];

                        const available_sizes = json["product_sizes**"].split(",");
    
                        inventories.forEach(inv=>{
                            const idx = available_sizes.findIndex(s=>{ return s === inv.size; });
                            if (idx === -1)
                                change_quantity(inv.id, 0);
                            else
                                set_add_to_cart_url(inv.id);
                        });
                        
                        return resolve(generate_return_object(inventories, product_url, product_name, product_gender, product_colorways.split(',')));
                    }).catch(err=>{
                        add_cart(cursor + 1);
                    });
                };
                
                add_cart(0);
            }).catch(err=>{
                //reject(generate_error_object(typeof(err) === "string" ? err : JSON.stringify(err)));
//console.log(store_id, product_id, err.status);
                resolve(generate_error_object("exception"));
            });
        };
        
        do_get(0);
    });
}

function fetchAllInventory(product_id){
    return new Promise((resolve, reject)=>{
        const tasks = stores.map(c=>{
            return fetchInventory(c.id, product_id);
        });
        Promise.all(tasks).then(objs=>{
            const ret = { product: product_id };

            const skus = [];
            objs.forEach(obj=>{
                obj.inventories.forEach(inv=>{
                    if (skus.findIndex(sku=>{ return sku.id == inv.id; }) == -1)
                        skus.push({ id: inv.id, stores: [] });
                });
            });
            
            skus.sort((i1, i2)=>{
                return i1.id.localeCompare(i2.id);
            });
            
            skus.forEach(sku=>{
                objs.forEach((obj, i)=>{
                    const store_id = stores[i].id;
                    const store_obj = obj.inventories.find(inv=>{ return inv.id == sku.id; });
                    if (obj.error)
                        sku.stores.push({
                            store: store_id,
                            error: obj.error
                        });
                    else if (store_obj){
                        const push_o = {
                            store: store_id,
                            size: store_obj.size,
                            sizeUnit: store_obj.sizeUnit,
                            currency: store_obj.currency,
                            dollar: store_obj.dollar
                        };
                        if (store_obj.inStock)
                            push_o.inStock = store_obj.inStock;
                        if (store_obj.quantity)
                            push_o.quantity = store_obj.quantity;
                        if (store_obj.price)
                            push_o.price = store_obj.price;
                        if (store_obj.priceSale)
                            push_o.priceSale = store_obj.priceSale;
                        if (store_obj.addToCartUrl)
                            push_o.addToCartUrl = store_obj.addToCartUrl;
                        if (obj.productUrl)
                            push_o.productUrl = obj.productUrl;
                        sku.stores.push(push_o);
                    }
                    else {
                        sku.stores.push({
                            store: store_id,
                            error: "no stock"
                        });
                    }

                    if (obj.name && !ret.name)
                        ret.name = obj.name;
                    if (obj.gender && !ret.gender)
                        ret.gender = obj.gender;
                    if (obj.colorways && !ret.colorways)
                        ret.colorways = obj.colorways;
                    if (obj.image && !ret.image)
                        ret.image = obj.image;
                });
            });
            
            ret.inventories = skus;
            if (skus.length === 0)
                ret.image = getImageUrl(stores[0].id, product_id);
            resolve(ret);
        }).catch(err=>{
            reject("error");
        });
    });
}

module.exports = {
    getSupportedStore: getSupportedStore,
    fetchAllProductId: fetchAllProductId,
    fetchInventory: fetchInventory,
    fetchAllInventory: fetchAllInventory
}
