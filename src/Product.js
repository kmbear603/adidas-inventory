import React, { Component } from 'react';
import { Grid, Row, Col } from "react-bootstrap";
import { Panel, Table } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { Glyphicon } from "react-bootstrap";

export default class Product extends Component {
    constructor(props){
        super(props);

        if (props.state)
            this.state = props.state;
        else
            this.state = { loading: true };
    }

    componentWillReceiveProps(new_props){
        const { state } = new_props;
        
        this.setState({
            loading: state ? state.loading : this.state.loading,
            error: state ? state.error : this.state.error,
            json: state ? state.json : this.state.json
        });
    }

    componentDidMount(){
        if (this.state.loading)
            this.update();
    }

    render(){
        const { productId, currency, exchangeRate } = this.props;
        const { loading, error, json, expanded } = this.state;

        var image = json ? json.image : null;
        if (!image && json && json.inventories){
            json.inventories.forEach(inv=>{
                if (!image && inv.stores){
                    inv.stores.forEach(store=>{
                        if (!image && !store.error)
                            image = store.image;
                    });
                }
            });
        }
        const inventories = json ? json.inventories : null;
        const gender = json ? json.gender : null;
        const name = json ? json.name : null;

        var header_dom;
        if (loading)
            header_dom = (
                <table>
                    <tr>
                        <td>{ productId }</td>
                    </tr>
                </table>
            );
        else {
            header_dom = (
                <table width={ "100%" }>
                    <tbody>
                        <tr>
                            <td>
                                <span style={ { fontSize: "large" } }><strong>{ productId }</strong></span>
                                {
                                    gender || name ?
                                        <span style={ { fontSize: "x-small" } }>
                                            <br/>
                                            { gender ? gender : null }
                                            { name ? (gender ? " - " : "") + name : null }
                                        </span>
                                    : null
                                }
                            </td>
                            <td style={ { textAlign: "right" } }>
                                <Button bsStyle="link" onClick={ this.update.bind(this) }>
                                    <Glyphicon glyph="refresh" />
                                </Button>
                                <Button bsStyle="link" onClick={ this.remove.bind(this) }>
                                    <Glyphicon glyph="remove" />
                                </Button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            );
        }

        var image_dom;
        if (loading || error)
            image_dom = null;
        else if (image)
            image_dom = <img src={ image } width={ "100%" } />;
        else
            image_dom = "No image";

        const summary = {};
        var summary_dom;
        if (loading || error)
            summary_dom = null;
        else {
            var summary_count = 0;
            var total_ct = 0;
            var more = false;

            inventories.forEach(inv=>{
                var ct = 0;
                if (inv.stores){
                    inv.stores.forEach(s=>{
                        if (!summary.hasOwnProperty(s.store)){
                            summary[s.store] = {
                                count: 0,
                                seq: summary_count,
                                more: false
                            };
                            summary_count++;
                        }
                                
                        if (s.quantity){
                            ct += s.quantity;
                            summary[s.store].count += s.quantity;
                        }
                        
                        if (s.inStock){
                            summary[s.store].more = true;
                            more = true;
                        }
                            
                        if (s.productUrl)
                            summary[s.store].productUrl = s.productUrl;

                        if (s.price && !summary[s.store].price){
                            summary[s.store].price = s.price;
                            summary[s.store].dollar = s.dollar;
                            summary[s.store].currency = s.currency;
                            if (s.priceSale)
                                summary[s.store].priceSale = s.priceSale;
                        }
                    });
                }
                total_ct += ct;
            });

            var summary_dom = [];
            for (var seq = 0; seq < summary_count; seq++){
                var this_summary = null;
                var store = null;
                for (var s in summary){
                    if (seq === summary[s].seq){
                        store = s;
                        this_summary = summary[s];
                        break;
                    }
                }
                if (!this_summary || this_summary.count == 0)
                    continue;
                
                var dom;
                if (this_summary.priceSale)
                    dom = (
                        <tr>
                            <td style={ { textAlign: "center" } }>{ store.toUpperCase() }</td>
                            <td style={ { textAlign: "center" } }>
                                <span style={ { color: "red" } }><span style={ { fontSize: "x-small" } }>{ currency ? currency : this_summary.currency }</span> { currency ? this.convertCurrency(this_summary.currency, this_summary.priceSale, exchangeRate) : this_summary.priceSale }</span>
                                { " " }
                                <strike><span style={ { fontSize: "x-small" } }>{ currency ? this.convertCurrency(this_summary.currency, this_summary.price, exchangeRate) : this_summary.price }</span></strike>
                            </td>
                            <td style={ { textAlign: "center" } }>
                                {
                                    (this_summary.count || this_summary.more) && this_summary.productUrl ?
                                        <a href={ this_summary.productUrl } target="_blank">
                                            <Glyphicon glyph="new-window" />
                                        </a>
                                    :
                                        null
                                }
                            </td>
                        </tr>
                    );
                else
                    dom = (
                        <tr>
                            <td style={ { textAlign: "center" } }>{ store.toUpperCase() }</td>
                            { false && <td style={ { textAlign: "center" } }>{ this_summary.count }</td> }
                            <td style={ { textAlign: "center" } }><span style={ { fontSize: "x-small" } }>{ currency ? currency : this_summary.currency }</span> { currency ? this.convertCurrency(this_summary.currency, this_summary.price, exchangeRate) : this_summary.price }</td>
                            <td style={ { textAlign: "center" } }>
                                {
                                    this_summary.count && this_summary.productUrl ?
                                        <a href={ this_summary.productUrl } target="_blank">
                                            <Glyphicon glyph="new-window" />
                                        </a>
                                    :
                                        null
                                }
                            </td>
                        </tr>
                    );

                summary_dom.push(dom);
            }

            summary_dom = (
                <Table condensed>
                    <tbody>
                        <tr>
                            <td style={ { textAlign: "center" } }>Total</td>
                            <td style={ { textAlign: "center" } }>{ total_ct }{ more ? "+" : null }</td>
                            <td style={ { textAlign: "center" } }></td>
                        </tr>
                        { summary_dom }
                    </tbody>
                </Table>
            );
        }

        var inventory_dom;
        if (loading)
            inventory_dom = <div>Loading...</div>;
        else if (error)
            inventory_dom = <div>Error!</div>;
        else {
            var total_ct = 0;
            inventories.forEach(inv=>{
                if (inv.stores){
                    inv.stores.forEach(s=>{
                        if (s.quantity)
                            total_ct += s.quantity;
                    });
                }
            });

            const header_dom = [];
            const inventory_element_dom = [];
            if (total_ct > 0){
                var written_header = false;
                
                inventories.forEach(inv=>{
                    var ret = null;
                    var this_size_exist = false;
                    
                    if (inv.stores){
                        const tmp = [];
                        inv.stores.forEach(store=>{
                            if (!written_header){
                                var this_header_dom;
                                if (summary[store.store] && summary[store.store].count == 0)
                                    this_header_dom = (
                                        <th style={ { textAlign: "center" } }>
                                            <font color="silver">
                                                { store.store.toUpperCase() }
                                            </font>
                                        </th>
                                    );
                                else
                                    this_header_dom = (
                                        <th style={ { textAlign: "center" } }>
                                            <a style={ { cursor: "pointer" } } onClick={ this.expandOrContract.bind(this) }>
                                                { store.store.toUpperCase() } [{ summary[store.store].count }{ summary[store.store].more ? "+" : null }]
                                            </a>
                                        </th>
                                    );
                                header_dom.push(this_header_dom);
                            }
                             
                            if (expanded){
                                if (!store.error && (store.quantity > 0 || store.inStock)){
                                    const core_dom = (
                                        <div>
                                            <span style={ { fontSize: "large" } }>
                                                { store.quantity ? store.quantity : <Glyphicon glyph="option-horizontal" /> }
                                            </span>
                                            <br/>
                                            <span style={ { fontSize: "small" } }>
                                                { store.size }
                                            </span>
                                            <br/>
                                        </div>
                                    );
                                    
                                    var wrapped_dom;
                                    
                                    if (store.addToCartUrl){
                                        wrapped_dom = (
                                            <Button bsStyle="link" onClick={ this.addToCart.bind(this, store.addToCartUrl) }>
                                                { core_dom }
                                            </Button>
                                        );
                                    }
                                    else if (store.productUrl){
                                        wrapped_dom = (
                                            <Button bsStyle="link" onClick={ this.navigate.bind(this, store.productUrl) }>
                                                { core_dom }
                                            </Button>
                                        );
                                    }
                                    else
                                        wrapped_dom = (
                                            <Button bsStyle="link" style={ { cursor: "default", color: "black" } }>
                                                { core_dom }
                                            </Button>
                                        );
                                        
                                    tmp.push(
                                        <td style={ { textAlign: "center" } }>
                                            { wrapped_dom }
                                        </td>
                                    );
                                    
                                    this_size_exist |= (store.quantity || store.inStock ? true : false);
                                }
                                else
                                    tmp.push(<td style={ { textAlign: "center" } }></td>);
                            }
                        });
                        
                        written_header = true;
                        
                        if (this_size_exist > 0 && tmp.length > 0){
                            ret = (
                                <tr>
                                    { tmp }
                                </tr>
                            );
                        }
                    }
                    
                    if (ret)
                        inventory_element_dom.push(ret);
                });
            }

            inventory_dom = total_ct > 0 ? (
                <Table condensed style={ { tableLayout: "fixed" } }>
                    <thead>
                        { false && <tr>
                            <th colSpan={ header_dom.length } style={ { textAlign: "center" } }>
                                <Button bsStyle="link" onClick={ this.expandOrContract.bind(this) }>
                                    <Glyphicon glyph={ expanded ? "chevron-up" : "chevron-down" } />
                                </Button>
                            </th>
                        </tr> }
                        <tr>
                            { header_dom }
                        </tr>
                    </thead>
                    <tbody>
                        { inventory_element_dom }
                    </tbody>
                </Table>
            ) : null;
        }

        return (
            <Panel className="product-panel" header={ header_dom }>
                <Grid fluid>
                    <Row>
                        <Col xs={ 4 } style={ { padding: 0, spacing: 0 } }>
                            { image_dom }
                        </Col>
                        <Col xs={ 8 } style={ { padding: 0, spacing: 0 } }>
                            { summary_dom }
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={ 12 } style={ { padding: 0, spacing: 0 } }>
                            { inventory_dom }
                        </Col>
                    </Row>
                </Grid>
            </Panel>
        );
    }
    
    convertCurrency(currency, price, rates){
        if (currency == rates.base)
            return Math.round(price * 100) / 100;
        else if (rates.hasOwnProperty(currency))
            return Math.round(price / rates[currency] * 100) / 100;
        else
            return NaN;
    }
    
    expandOrContract(){
        const { expanded } = this.state;
        this.setState({ expanded: expanded ? false : true });
    }
    
    addToCart(url){
        this.props.onAddToCart(url);
    }
    
    navigate(url){
        this.props.onNavigateToProductPage(url);
    }
    
    remove(){
        const { productId } = this.props;
        this.props.onRemove(productId);
    }

    update(){
        const { productId } = this.props;
        this.props.onUpdate(productId);
    }
    
    updateXX(){
        this.setState({ loading: true });
            
        const { productId, currency } = this.props;

        window.superagent.get("/api/all/" + productId).then(res=>{
            const json = JSON.parse(res.text);

            this.setState({
                loading: false,
                error: false,
                json: json
            }, ()=>{
                this.props.onUpdated(productId, this.state);
            });
        }).catch (err=>{
            this.setState({
                loading: false,
                error: true
            });
        });
    }
}

