import React, { Component } from 'react';
import { Button, Form, FormGroup, ControlLabel, FormControl } from "react-bootstrap";
import { Well, Row, Col } from "react-bootstrap";

export default class AddProductForm extends Component {
    constructor(){
        super();
        this.state = { inputText: "" };
    }
    
    render(){
        return (
            <Row>
                <Col xs={ 12 } sm={ 8 } md={ 6 } lg={ 4 }>
                    <Form inline onSubmit={ this.add.bind(this) }>
                        <FormControl type="text" placeholder="Product ID, eg. BA8842" onChange={ this.onEditText.bind(this) } value={ this.state.inputText } />
{ false &&                        <Button type="submit" bsStyle="primary">Add</Button> }
                    </Form>
                </Col>
            </Row>
        );
    }

    onEditText(e){
        this.setState({
            inputText: e.target.value.trim().toUpperCase()
        });
    }

    add(e){
        e.preventDefault();
        if (this.state.inputText.length == 0)
            return false;
        
        this.props.onAddProduct(this.state.inputText);
        this.setState({
            inputText: ""
        });
        return false;
    }
}

