import React, { Component } from 'react';
import AddProductForm from "./AddProductForm.js";
import Product from "./Product.js";
import { Grid, Row, Col } from "react-bootstrap";
import { Button } from "react-bootstrap";

const DEFAULT_ITEM_PER_PAGE = 15;
    
export default class App extends Component {
    constructor(){
        super();
        
        this.state = {
            products: [],
            loading: true,
            page: 1,
            itemPerPage: DEFAULT_ITEM_PER_PAGE,
            shownCartTips: false
        };
    }
    
    componentDidMount(){
        this.initFromPath().then(()=>{
            this.setState({ loading: false }, ()=>{
                this.updatePath(true);
            });
        });
        
        window.onpopstate = (e)=>{
            if (e.state.page)
                this.setState({ page: e.state.page });
        };
    }
    
    render(){
        const { loading, page, itemPerPage, products, currency, exchangeRate } = this.state;

        const items = [];
        var product_dom;
        if (loading)
            product_dom = <div>Loading...</div>;
        else if (products.length > 0){
            var count = 0;
            var items_in_col = [];
            for (var i = (page - 1) * itemPerPage; i < Math.min(products.length, page * itemPerPage); i++){
                const p = products[i];

                items.push(
                    <Col xs={ 12 } style={ { margin: 0, padding: 0 } }>
                        <Product key={ p.id }
                            productId={ p.id }
                            state={ p.state }
                            currency={ currency }
                            exchangeRate={ exchangeRate }
                            onRemove={ this.removeProduct.bind(this) }
                            //onUpdated={ this.updateProduct.bind(this) }
                            onUpdate={ this.updateProduct.bind(this) }
                            onAddToCart={ this.onClickAddToCart.bind(this) }
                            onNavigateToProductPage={ this.onNavigateToProductPage.bind(this) }
                        />
                    </Col>
                );
                
                count++;
            }
            
            product_dom = (
                <Grid fluid>
                    <Row>
                        { items }
                    </Row>
                </Grid>
            );
        }
        else
            product_dom = <div>Nothing</div>;

        var pagination_dom;
        if (loading || products.length == 0)
            pagination_dom = null;
        else {
            var prev_page_dom;
            if (page == 1)
                prev_page_dom = null;
            else
                prev_page_dom = <Button bsStyle="link" onClick={ this.onClickPreviousPage.bind(this) }>Previous</Button>;

            var next_page_dom;
            if (page * itemPerPage >= products.length)
                next_page_dom = null;
            else
                next_page_dom = <Button bsStyle="link" onClick={ this.onClickNextPage.bind(this) }>Next</Button>;

            pagination_dom = (
                <Grid fluid>
                    <Row>
                        <Col xs={ 4 } style={ { margin: 0, padding: 0 } }>
                            { prev_page_dom }
                        </Col>
                        <Col xs={ 4 } style={ { textAlign: "center", margin: 0, padding: 0 } }>
                            <Button bsStyle="link" style={ { color: "black" } } onClick={ this.onClickFirstPage.bind(this) }>
                                { "Page " }{ page }{ " of " }{ Math.ceil(products.length / itemPerPage) }
                            </Button>
                        </Col>
                        <Col xs={ 4 } style={ { textAlign: "right", margin: 0, padding: 0 } }>
                            { next_page_dom }
                        </Col>
                    </Row>
                </Grid>
            );
        }

        return (
            <Grid fluid>
                <Row>
                    <Col xs={ 12 } sm={ 10 } smOffset={ 1 } md={ 8 } mdOffset={ 2 } lg={ 6 } lgOffset={ 3 }>
                        <Grid fluid>
                            <Row>
                                <Col xs={ 12 } style={ { padding: 0, margin: 0 } }>
                                    <AddProductForm onAddProduct={ this.addProduct.bind(this) } style={ { margin: 0, padding: 0 } } />
                                </Col>
                            </Row>
                        </Grid>

                        { pagination_dom ? <br/> : null }                
                        { pagination_dom }

                        { product_dom ? <br/> : null }
                        { product_dom }
                        
                        { pagination_dom ? <br/> : null }
                        { pagination_dom }
                    </Col>
                </Row>
                
                <br />
                <br />
                <br />
                <br />
            </Grid>
        );
    }

    onClickAddToCart(url){
        const go = ()=>{
            window.open(url, "_blank").focus();
        };
        
        const { shownCartTips } = this.state;
        
        if (!shownCartTips){
            this.setState({ shownCartTips: true}, ()=>{
                go();
            });
        }
        else
            go();
    }
    
    onNavigateToProductPage(url){
        window.open(url, "_blank").focus();
    }

    createProduct(product_id){
        return { id: product_id };
    }

    onClickFirstPage(){
        this.setState({ page: 1 }, ()=>{
            this.updatePath();
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        });
    }
    
    onClickPreviousPage(){
        this.setState({ page: this.state.page - 1 }, ()=>{
            this.updatePath();
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        });
    }

    onClickNextPage(){
        this.setState({ page: this.state.page + 1 }, ()=>{
            this.updatePath();
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        });
    }

    initFromPath(){
        return new Promise((resolve, reject)=>{
            var new_state = {};
            
            const do_final_job = ()=>{
                if (new_state.page){
                    const min_page = 1;
                    const max_page = Math.ceil(new_state.products.length / this.state.itemPerPage);
                    if (new_state.page > max_page)
                        new_state.page = max_page;
                    else if (new_state.page < min_page)
                        new_state.page = min_page;
                }

                const done = ()=>{
                    this.setState(new_state, ()=>{
                        resolve();
                    });
                };

                if (new_state.currency)
                    this.updateExchangeRate(new_state.currency).then(()=>{
                        done();
                    });
                else
                    done();
            };
            
            const query = window.location.search.substr(1);
            const vars = query.split('&');
            for (var i = 0; i < vars.length; i++) {
                const pair = vars[i].split('=');
                const key = decodeURIComponent(pair[0]).trim().toLowerCase();
                const val = decodeURIComponent(pair[1]).trim();
                if (key === "page" && !isNaN(parseInt(val)))
                    new_state.page = parseInt(val);
                else if (key === "currency")
                    new_state.currency = val.toUpperCase();
            }
            
            const local_path = window.location.pathname.substr(1);
            if (local_path === ""){
                new_state.products = [];
                return do_final_job();
            }
            else if (local_path.toLowerCase() === "ultraboost"){
                window.superagent.get("/api/product/ultraboost").then(res=>{
                    const pids = JSON.parse(res.text);
                    new_state.products = pids.map(pid=>{ return this.createProduct(pid); });
                    return do_final_job();
                });
            }
            else {
                var input_ids = local_path.split(',').filter(id=>{ return id.trim().length > 0; });
                const map = {};
                const ids = input_ids.filter(id=>{
                    if (map.hasOwnProperty(id))
                        return false;
                    map[id] = true;
                    return true;
                });
                new_state.products = ids.map(id=>{ return this.createProduct(id.trim().toUpperCase()); });
                return do_final_job();
            }
        });
    }

    updateExchangeRate(currency){
        return new Promise((resolve, reject)=>{
            currency = currency || this.state.currency;
            if (!currency)
                return resolve();
        
            window.superagent.get("/api/currency/" + currency).then(res=>{
                const json = JSON.parse(res.text);
                this.setState({ exchangeRate: json }, ()=>{
                    resolve();
                });
            }).catch(err=>{
                alert("error");
                reject(err);
            });
        });
    }

    updatePath(replace){
        const { products, page, currency } = this.state;
        
        var query = "";
        const append_query = (key, val)=>{
            if (query !== "")
                query += "&";
            query += key + "=" + val;
        };
        if (page > 1)
            append_query("page", page);
        if (currency)
            append_query("currency", currency);
        
        const state = { page: page };
        const title = "Adidas Inventory" + (page > 1 ? " [page " + page + "]" : "");
        const new_url = "/" + products.map(p=>{ return p.id ; }).join(',') + (query.length > 0 ? "?" + query : "");
        if (replace)
            window.history.replaceState(state, title, new_url);
        else
            window.history.pushState(state, title, new_url);
    }

    addProduct(product_id){
        const { products } = this.state;
        if (products.findIndex(p=>{ return p.id === product_id; }) !== -1)
            return alert("already exist");
        products.push(this.createProduct(product_id));
        this.setState({ products: products }, ()=>{
            this.updatePath(true);
        });
    }

    removeProduct(product_id){
        var { products } = this.state;
        products = products.filter(p=>{
            return p.id !== product_id;
        });
        this.setState({ products: products }, ()=>{
            this.updatePath(true);
        });
    }
    
    updateProductXX(product_id, state){
        const { products } = this.state;
        const product = products.find(p=>{
            return p.id === product_id;
        });
        if (!product)
            return;
        product.state = state;
        this.setState({ products: products });
    }
    
    updateProduct(product_id){
        const set_product_state = (s, callback)=>{
            const { products } = this.state;
            const product = products.find(p=>{
                return p.id === product_id;
            });
            if (!product)
                return;
                
            product.state = s;
//console.log("before", product_id, products.find(p=>{ return p.id === "BB3047";}));
            this.setState({ products: products }, ()=>{
                const products2 = this.state.products;
//console.log("after", product_id, products2.find(p=>{ return p.id === "BB3047";}));
                callback();
            });
        };

        set_product_state({ loading: true }, ()=>{
            window.superagent.get("/api/all/" + product_id).then(res=>{
                const json = JSON.parse(res.text);
                set_product_state({ json: json }, ()=>{
                    
                });
            }).catch (err=>{
                set_product_state({ error: true }, ()=>{});
            });
        });
    }
}

